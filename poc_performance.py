from requests import Request, Session
import yaml
import time
from datetime import datetime
import os
import pathlib
import sys
from PIL import Image

username="zabbix-dev"
password="Ahtaihei5iey"
login_post_data={
    'name':username,
    'password':password,
    'enter':'Sign in',
    'autologin':'1',
    'request':''}

url_basic = "http://10.187.0.102/zabbix/"
start = time.gmtime()
start_time = time.strftime("%Y%m%d%H%M%S", start)
url_login="{}index.php?login=1".format(url_basic)

def prepare_graph_url_for_disk(url_basic, graph_id, duration_time, start_time):
    return "{}chart6.php?graphid={}&period={}&stime={}&sid=446b9222d4bdc16b".format(url_basic, graph_id, duration_time, start_time)

def prepare_graph_url(url_basic, graph_id, duration_time, start_time):
    return "{}chart2.php?graphid={}&period={}&stime={}&width=1717".format(url_basic, graph_id, duration_time, start_time)


def save_image(image, path):
    with open(path, 'wb') as f:
        for block in image.iter_content(1024):
            f.write(block)

#run jmeter tests
time.sleep(2) #TODO call jmeter tests

#count duration
finish = time.gmtime()
duration_time = int(time.mktime(time.gmtime()) - time.mktime(start))
if duration_time < 60: #minimum period accepted by zabbix
    duration_time = 60

#authenticate
session = Session()
login_request = Request('POST', url_login, data=login_post_data).prepare()
login_response_cookie = session.send(login_request).cookies

#get metrics
with open ('hosts.yaml') as f:
    hosts = yaml.safe_load(f)
for host, service in hosts.items():
    saved_pictures = []
    for graph, number in service.items():
        filename = "{}_{}.png".format(host,graph)
        if graph == "disk":
            request = Request('GET',prepare_graph_url_for_disk(url_basic,number,duration_time, start_time), cookies = login_response_cookie).prepare()
        else:
            request = Request('GET',prepare_graph_url(url_basic,number,duration_time, start_time), cookies = login_response_cookie).prepare()
        response = session.send(request)
        save_image(response, filename)
        saved_pictures.append(filename)

    #merge saved files
    images = map(Image.open, saved_pictures)
    widths, heights = zip(*(i.size for i in images))
    total_height = sum(heights)
    max_width = max(widths)

    merged_picture = Image.new('RGB', (max_width, total_height), "white")

    y_offset = 0
    for im in images:
      merged_picture.paste(im, (0,y_offset))
      y_offset += im.size[1]

    merged_picture.save("{}.png".format(host))
